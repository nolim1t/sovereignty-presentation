# Sovereignty Meetup Slides

## How to use (locally)

1. Clone this Repo
2. Enter the repo directory
3. Issue the following command

```bash
npm start
```

4. Go to http://localhost:8000

## How to use (Remote)

Just go to [This page](https://sovmeetup.nolim1t.co/)

## Forking Guide

1. Fork/Clone this Repo
2. Edit the about to suit you.
3. Edit whatever information you would like but lets try to keep the same format
